<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class BackendController extends Controller
{

    public function uploadImage(Request $request){
        $type = $request->input('type','items');
        $image = $request->file('image');
        $imgWidth = $request->input('imgWidth',600);
        $imgHeight = $request->input('imgHeight',null);
        if(!$image) {
            return response()->json('上传错误',422);
        };
        $imageDir = public_path('images/'.$type.'/');
        if($image->isValid()){
            if(!is_dir($imageDir)){
                mkdir($imageDir);
            }
            $imageName = date('His') .random_int(1000,9999).'.jpg';
            \Image::make($image)->resize($imgWidth,$imgHeight,function($constraint){
                $constraint->aspectRatio();
            })->save($imageDir.$imageName);
            return response()->json(['link' => '/images/'.$type.'/'.$imageName]);
        }
        return response()->json('上传错误',422);
    }

    public function delImage(){
        $imge = 2;
    }

}