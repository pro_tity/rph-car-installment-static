/**
 * Created by TBZ on 2017/10/10.
 */
import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        {
            path:'/index',
            component: require('../views/log.vue')
        }
    ]
})