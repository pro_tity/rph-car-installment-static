// window.Vue = require('vue');
import Vue from 'vue/dist/vue.common.js'
import App from '../views/App.vue'
import router from './router'

const app = new Vue({
    el:"#app",
    router,
    render:h => h(App)
});